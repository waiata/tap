//
//  ReactConsole.swift
//  Tap
//
//  Created by Neal Watkins on 2021/6/25.
//

import Cocoa

class ReactConsole: NSViewController {

    //MARK: Outlets
    
    @IBOutlet weak var tapButton: NSButton!
    
    @IBOutlet weak var lightsIndicator: NSLevelIndicator!
    @IBOutlet weak var reactionsTable: NSTableView!
    
    @IBOutlet weak var meanLabel: NSTextField!
    
    
    //MARK: Actions
    
    @IBAction func tapped(_ sender: Any) {
        
        let now = Date()
        guard let out = lightsOut else {
            falseStart()
            return
        }
        let reaction = now.timeIntervalSince(out)
        add(reaction)
        render(reaction)
        zero()
        reLight()
    }
    
    @IBAction func clear(_ sender: Any) {
        reactions = []
        reactionsTable.reloadData()
        render()
        zero()
    }
    
    @IBAction func ready(_ sender: Any) {
        readyLights()
    }
    
    //MARK: Reactions
    
    typealias Reaction = TimeInterval?
    
    var reactions = [Reaction]()

    var lightsOut: Date?
    
    var ticker = Timer()
    
    func add(_ reaction: Reaction) {
        reactions.append(reaction)
        let row = reactions.count - 1
        reactionsTable.insertRows(at: IndexSet(integer: row), withAnimation: .slideDown)
        reactionsTable.scroll(reactionsTable.rect(ofRow: row).origin)
    }
    
    func render(_ reaction: Reaction? = nil) {
        if let reaction = reaction {
            tapButton.title = "\(sec(reaction))"
        } else {
            tapButton.title = "Go"
        }
        let mean = mean
        meanLabel.stringValue = sec(mean)
    }
    
    /// reset sequence
    func zero() {
        ticker.invalidate()
        lightsOut = nil
        lightsOn = 0
        lightsIndicator.maxValue = Double(lightCount)
    }
    
    /// autoReady?
    func reLight() {
        if autoReady {
            readyLights()
        }
    }
    
    /// pressed too early!
    func falseStart() {
        NSSound.beep()
        tapButton.title = "False Start!"
        add(nil)
        zero()
        reLight()
    }
    
    /// set up indicators
    func readyLights() {
        zero()
        stepLights()
    }
    
    func stepLights() {
        var period = lightPeriod
        if allLightsOn {
            period = Double.random(in: lightRandomMin...lightRandomMax)
        }
        ticker = Timer.scheduledTimer(withTimeInterval: period, repeats: false) { _ in
            self.nextLight()
        }
    }
    
    func nextLight() {
        if allLightsOn {
            lightsOut = Date()
            lightsOn = 0
        } else {
            lightsOut = nil
            lightsOn += 1
            stepLights()
        }
        
    }
    
    var lightsOn: Int {
        get {
            return lightsIndicator.integerValue
        }
        set {
            lightsIndicator.integerValue = newValue
        }
    }
    
    var allLightsOn: Bool {
        return lightsIndicator.doubleValue == lightsIndicator.maxValue
    }
    
    /// average reaction time
    var mean: Reaction? {
        let count = Double(reactions.count)
        guard count > 0 else { return nil }
        let sum = reactions.compactMap{ $0 }.reduce(0, +)
        return sum / count
    }
    
    ///formatted time
    func sec(_ reaction: Reaction? = nil) -> String {
        guard let t = reaction as? Double else { return "" }
        return String(format: "%.3f", t)
    }
    
    //MARK: Defaults
    
    var lightCount: Int {
        return UserDefaults.standard.integer(forKey: "LightCount")
    }
    
    var lightPeriod: TimeInterval {
        return UserDefaults.standard.double(forKey: "LightPeriod")
    }
    
    var lightRandomMin: TimeInterval {
        return UserDefaults.standard.double(forKey: "LightRandomMin")
    }
    
    var lightRandomMax: TimeInterval {
        return UserDefaults.standard.double(forKey: "LightRandomMax")
    }
    
    var autoReady: Bool {
        return UserDefaults.standard.bool(forKey: "AutoReady")
    }
    
    //MARK: Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reactionsTable.delegate = self
        reactionsTable.dataSource = self
    }
    
}


extension ReactConsole: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return reactions.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let reaction = reactions[row]
        switch tableColumn?.identifier {
        case Table.react.column : return react(view: reaction)
        default : return nil
        }
    }
    
    func react(view reaction: Reaction) -> NSView? {
        let view = reactionsTable.makeView(withIdentifier: Table.react.cell, owner: nil) as? NSTableCellView
        if let t = reaction {
            view?.textField?.stringValue = sec(t)
        } else {
            view?.textField?.stringValue = "!"
            view?.imageView?.image = NSImage(systemSymbolName: "exclamationmark.octagon.fill", accessibilityDescription: "False Start")
        }
        return view
    }
    
   
    
    struct Table {
        
        struct react {
            static let column = NSUserInterfaceItemIdentifier("react Column")
            static let cell = NSUserInterfaceItemIdentifier("react Cell")
        }
        
    }
    
}
