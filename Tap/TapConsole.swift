//
//  TapConsole.swift
//  Tap
//
//  Created by Neal Watkins on 2021/5/27.
//

import Cocoa


class TapConsole: NSViewController {

    @IBOutlet weak var tapButton: NSButton!
    @IBOutlet weak var playButton: NSButton!
    
    @IBOutlet weak var tapsTable: NSTableView!
    
    @IBOutlet weak var secLabel: NSTextField!
    @IBOutlet weak var bpmLabel: NSTextField!
    @IBOutlet weak var frameLabel: NSTextField!
    
    @IBAction func tapped(_ sender: Any) {
        
        let now = Date()
        guard let last = lastTap else {
            lastTap = now
            return
        }
        let tap = now.timeIntervalSince(last)
        lastTap = now
        add(tap)
        render(tap)
    }
    
    @IBAction func clear(_ sender: Any) {
        taps = []
        lastTap = nil
        tapsTable.reloadData()
        render()
    }
    
    @IBAction func togglePlayback(_ sender: Any) {
        if ticker.isValid {
            stopPlayback()
        } else {
            play(0)
        }
    }
    
    typealias Tap = TimeInterval
    
    var taps = [Tap]()

    var lastTap: Date?
    
    var ticker = Timer()
    
    func add(_ tap: Tap) {
        taps.append(tap)
        let row = taps.count - 1
        tapsTable.insertRows(at: IndexSet(integer: row), withAnimation: .slideDown)
        tapsTable.scroll(tapsTable.rect(ofRow: row).origin)
    }
    
    func render(_ tap: Tap? = nil) {
        if let tap = tap {
            tapButton.title = "\(sec(tap)) s\n\(bpm(tap)) bpm\n\(frames(tap)) f"
        } else {
            tapButton.title = ""
        }
        let mean = mean
        secLabel.stringValue = sec(mean)
        bpmLabel.stringValue = bpm(mean)
        frameLabel.stringValue = frames(mean)
    }
    
    /// playback tap at index
    func play(_ index: Int) {
        guard let i = loop(index: index) else {
            stopPlayback()
            return
        }
        let tap = taps[i]
        ticker = Timer.scheduledTimer(withTimeInterval: tap, repeats: false) { _ in
            self.play(i + 1)
        }
        tapsTable.selectRowIndexes([i], byExtendingSelection: false)
        playButton.state = .on
        flashButton()
        render(tap)

    }
    
    func loop(index: Int) -> Int? {
        guard !taps.isEmpty else { return nil }
        var i = index
        if playbackLoops { i = index % taps.count }
        guard taps.indices.contains(i) else { return nil }
        return i
    }
    
    func stopPlayback() {
        ticker.invalidate()
        tapsTable.selectRowIndexes([], byExtendingSelection: false )
        playButton.state = .off
    }
    
    func flashButton() {
        tapButton.highlight(true)
        Timer.scheduledTimer(withTimeInterval: flashDuration, repeats: false) { _ in
            self.tapButton.highlight(false)
        }
    }
    
    var mean: Tap? {
        let count = Double(taps.count)
        guard count > 0 else { return nil }
        let sum = taps.reduce(0, +)
        return sum / count
    }
    
    func sec(_ tap: Tap? = nil) -> String {
        guard let tap = tap else { return "" }
        return String(format: "%.3f", tap)
    }
    
    func bpm(_ tap: Tap? = nil) -> String {
        guard let tap = tap, tap > 0 else { return "" }
        let bpm = 60 / tap
        return String(format: "%.1f", bpm)
    }
    
    func frames(_ tap: Tap? = nil) -> String {
        guard let tap = tap else { return "" }
        let frames = frameRate * tap
        return String(format: "%.2f", frames)
    }
    
    //MARK: Defaults
    
    var frameRate: TimeInterval {
        return UserDefaults.standard.double(forKey: "FrameRate")
    }
    var flashDuration: TimeInterval {
        return UserDefaults.standard.double(forKey: "FlashDuration")
    }
    var playbackLoops: Bool {
        return UserDefaults.standard.bool(forKey: "LoopPlayback")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapsTable.delegate = self
        tapsTable.dataSource = self
    }

}

extension TapConsole: NSTableViewDelegate, NSTableViewDataSource {
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return taps.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let tap = taps[row]
        switch tableColumn?.identifier {
        case Table.sec.column : return sec(view: tap)
        case Table.bpm.column : return bpm(view: tap)
        case Table.frame.column : return frame(view: tap)
        default : return nil
        }
    }
    
    func sec(view tap: Tap) -> NSView? {
        let view = tapsTable.makeView(withIdentifier: Table.sec.cell, owner: nil) as? NSTableCellView
        view?.textField?.stringValue = sec(tap)
        return view
    }
    
    func bpm(view tap: Tap) -> NSView? {
        let view = tapsTable.makeView(withIdentifier: Table.bpm.cell, owner: nil) as? NSTableCellView
        view?.textField?.stringValue = bpm(tap)
        return view
    }
    
    func frame(view tap: Tap) -> NSView? {
        let view = tapsTable.makeView(withIdentifier: Table.frame.cell, owner: nil) as? NSTableCellView
        view?.textField?.stringValue = frames(tap)
        return view
    }
    
    struct Table {
        
        struct sec {
            static let column = NSUserInterfaceItemIdentifier("sec Column")
            static let cell = NSUserInterfaceItemIdentifier("sec Cell")
        }
        
        struct bpm {
            static let column = NSUserInterfaceItemIdentifier("bpm Column")
            static let cell = NSUserInterfaceItemIdentifier("bpm Cell")
        }
        
        struct frame {
            static let column = NSUserInterfaceItemIdentifier("frame Column")
            static let cell = NSUserInterfaceItemIdentifier("frame Cell")
        }
    }
    
}

