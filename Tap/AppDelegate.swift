//
//  AppDelegate.swift
//  Tap
//
//  Created by Neal Watkins on 2021/5/27.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        registerDefaults()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func registerDefaults() {
        let plist = Bundle.main.path(forResource: "Defaults", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: plist!) as? [String: Any] {
            UserDefaults.standard.register(defaults: dic)
        }
    }

}

